package com.faz.testapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.MvpAppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.faz.testapp.adapter.DetailsAdapter
import com.faz.testapp.data.ListItemData
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : MvpAppCompatActivity() {

    private val detailsAdapter by lazy { DetailsAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val item = intent.extras!!.getParcelable<ListItemData>(PARAMS_KEY)
        Glide.with(this)
                .load(item.iconUrl)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(iconView)
        nameView.text = item.partner.name

        detailsAdapter.fillWithItem(item)
        recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = detailsAdapter
        }
    }

    companion object {
        private const val PARAMS_KEY = "PARAMS_KEY"

        fun prepareLaunchIntent(context: Context, params: ListItemData): Intent {
            val args = Bundle().apply {
                putParcelable(PARAMS_KEY, params)
            }
            return Intent(context, DetailsActivity::class.java).apply {
                putExtras(args)
            }
        }
    }
}
