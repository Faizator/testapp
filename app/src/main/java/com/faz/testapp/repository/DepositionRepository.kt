package com.faz.testapp.repository

import com.faz.testapp.api.ApiService
import com.faz.testapp.api.data.DepositionPartnerDTO
import com.faz.testapp.api.data.DepositionPointDTO
import com.faz.testapp.common.NoCachedException
import com.faz.testapp.common.rx.SchedulerProvider
import com.faz.testapp.database.AppDatabase
import com.faz.testapp.database.entity.DepositionPartnersEntity
import com.faz.testapp.database.entity.DepositionPointsEntity
import com.faz.testapp.database.entity.SeenDepositionPointsEntity
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DepositionRepository @Inject constructor(
    private val api: ApiService,
    private val appDatabase: AppDatabase,
    private val schedulerProvider: SchedulerProvider
) {

    private val pointsDao by lazy { appDatabase.getDepositionPointsDao() }
    private val seenDao by lazy { appDatabase.getSeenDepositionPointsDao() }
    private val partnersDao by lazy { appDatabase.getDepositionPartnersDao() }

    @Volatile
    private var partnersMemoryCache = mapOf<String, DepositionPartnerDTO>()
    @Volatile
    private var seenMemoryCache = setOf<String>()

    fun points(latitude: Double, longitude: Double, radius: Long): Single<List<DepositionPointDTO>> =
        ensurePartnersCached()
            .andThen(ensureSeenCached())
            .andThen(
                pointsDao.getById(DepositionPointsEntity.calcId(latitude, longitude, radius))
                    .subscribeOn(schedulerProvider.io())
                    .map {
                        DepositionPointsEntity.deserializeData(it.data)
                    }
                    .onErrorResumeNext {
                        api.depositionPoints(latitude, longitude, radius)
                            .doOnSuccess {
                                pointsDao.insert(DepositionPointsEntity.from(latitude, longitude, radius, it))
                                    .subscribe({}, {})
                            }
                    }
                    .map { it.payload }
            )

    private fun ensurePartnersCached(): Completable = if (partnersMemoryCache.isEmpty()) {
        partnersDao.get()
            .subscribeOn(schedulerProvider.io())
            .map {
                DepositionPartnersEntity.deserializeData(it.data)
            }
            .onErrorResumeNext {
                api.depositionPartners()
                    .doOnSuccess {
                        partnersDao.insert(DepositionPartnersEntity.from(it))
                            .subscribe({}, {})
                    }
            }
            .map { it.payload }
            .observeOn(schedulerProvider.computation())
            .doOnSuccess { partners ->
                partnersMemoryCache = partners.associateBy { it.id }
            }
            .ignoreElement()
    } else {
        Completable.complete()
    }

    private fun ensureSeenCached(): Completable = if (seenMemoryCache.isEmpty()) {
        seenDao.getAll()
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.computation())
            .doOnSuccess { seen ->
                seenMemoryCache = seen.map { it.id }.toSet()
            }
            .ignoreElement()
    } else {
        Completable.complete()
    }

    fun partner(point: DepositionPointDTO): DepositionPartnerDTO {
        val id = point.partnerName
        val cached = partnersMemoryCache[id]
        return cached ?: throw NoCachedException()
    }

    fun seen(point: DepositionPointDTO): Boolean {
        val id = seenIdFromDepositionPointDTO(point)
        return seenMemoryCache.contains(id)
    }

    fun markAsSeen(point: DepositionPointDTO) {
        val location = point.location
        seenMemoryCache = seenMemoryCache + seenIdFromDepositionPointDTO(point)
        seenDao.insert(SeenDepositionPointsEntity.create(location.latitude, location.longitude, point.externalId))
            .subscribeOn(schedulerProvider.io())
            .subscribe()
    }

    private fun seenIdFromDepositionPointDTO(point: DepositionPointDTO): String {
        val location = point.location
        return SeenDepositionPointsEntity.calcId(location.latitude, location.longitude, point.externalId)
    }
}