package com.faz.testapp

import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import com.faz.testapp.adapter.MapListAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : MvpAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViewPager()
    }

    private fun setupViewPager() {
        tabs.setupWithViewPager(viewPager)
        viewPager.adapter = MapListAdapter(supportFragmentManager)
    }
}
