package com.faz.testapp

import android.app.Application
import com.facebook.stetho.Stetho
import com.faz.testapp.di.component.AppComponent
import com.faz.testapp.di.component.DaggerAppComponent

class App : Application() {

    lateinit var appComponent: AppComponent
        private set

    override fun onCreate() {
        super.onCreate()
        instance = this
        appComponent = DaggerAppComponent.create()
        Stetho.initializeWithDefaults(this)
    }

    companion object {
        lateinit var instance: App
            private set
    }
}