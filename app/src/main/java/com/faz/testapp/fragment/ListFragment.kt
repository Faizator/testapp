package com.faz.testapp.fragment

import android.app.ActivityOptions
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.faz.testapp.DetailsActivity
import com.faz.testapp.R
import com.faz.testapp.adapter.ListAdapter
import com.faz.testapp.common.kextension.appComponent
import com.faz.testapp.presenter.ListPresenter
import com.faz.testapp.view.ListView
import com.faz.testapp.data.ListItemData
import kotlinx.android.synthetic.main.fragment_list.*

class ListFragment : MvpAppCompatFragment(), ListView {

    @InjectPresenter
    lateinit var presenter: ListPresenter

    @ProvidePresenter
    fun providePresenter() = appComponent.listPresenter

    private val listAdapter by lazy {
        ListAdapter({ presenter.seen(it) }) { item, iconView ->
            presenter.onItemClick(item)
            val intent = DetailsActivity.prepareLaunchIntent(requireContext(), item)
            val options = ActivityOptions.makeSceneTransitionAnimation(activity, iconView, "icon")
            startActivity(intent, options.toBundle())
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = listAdapter
        }
    }

    override fun showPoints(points: List<ListItemData>) {
        listAdapter.items = points
    }

    override fun updateSeen() {
        listAdapter.notifyDataSetChanged()
    }

    companion object {
        fun newInstance() = ListFragment()
    }
}