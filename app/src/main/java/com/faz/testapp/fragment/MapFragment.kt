package com.faz.testapp.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener
import com.google.android.gms.maps.GoogleMap.OnCameraIdleListener
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.CameraPosition
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.faz.testapp.DetailsActivity
import com.faz.testapp.R
import com.faz.testapp.common.kextension.appComponent
import com.faz.testapp.common.kextension.checkAndRequestPermissions
import com.faz.testapp.common.kextension.hasAllRequiredPermissions
import com.faz.testapp.presenter.MapPresenter
import com.faz.testapp.view.MapView
import com.faz.testapp.data.ListItemData
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.fragment_map.*
import kotlinx.android.synthetic.main.list_item.*
import android.app.ActivityOptions
import com.faz.testapp.common.bottomsheet.StateChangedCallback

class MapFragment : MvpAppCompatFragment(), MapView, OnMapReadyCallback, OnMarkerClickListener, OnCameraIdleListener {

    private lateinit var map: GoogleMap
    private lateinit var bsBehavior: BottomSheetBehavior<FrameLayout>

    @InjectPresenter
    lateinit var presenter: MapPresenter

    @ProvidePresenter
    fun providePresenter() = appComponent.mapPresenter

    private val pointIds = mutableSetOf<String>()

    private var lastShownItem: ListItemData? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_map, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initMapFragment()
        initBsBehaviour()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        initMapControls()
        setMapListeners()
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        val item = marker.tag as ListItemData
        presenter.onItemSelect(item)
        return true
    }

    override fun onCameraIdle() = presenter.onVisibleRegionChanged(map.projection.visibleRegion)

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CODE && hasAllRequiredPermissions()) {
            enableMyLocation()
        }
    }

    override fun moveTo(location: LatLng) {
        val cameraPosition = CameraPosition.Builder()
            .target(location)
            .zoom(15f)
            .build()

        map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    override fun addDepositionPoints(points: List<ListItemData>) {
        points.forEach {
            val point = it.point
            if (pointIds.contains(point.externalId)) return@forEach
            pointIds.add(it.point.externalId)
            val location = LatLng(point.location.latitude, point.location.longitude)
            map.addMarker(
                MarkerOptions().position(location).title(point.partnerName)
            ).tag = it
        }
    }

    override fun showMarkerInfo(item: ListItemData) {
        lastShownItem = item
        bsBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        val partnerName = item.partner.name
        val address = item.point.fullAddress
        notseenView.visibility = if (presenter.seen(item)) View.INVISIBLE else View.VISIBLE
        nameView.text = partnerName
        addressView.text = address
        Glide.with(this)
            .load(item.iconUrl)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(iconView)
        bottomSheet.setOnClickListener {
            presenter.onItemClick(item)
            val intent = DetailsActivity.prepareLaunchIntent(requireContext(), item)
            val options = ActivityOptions.makeSceneTransitionAnimation(activity, iconView, "icon")
            startActivity(intent, options.toBundle())
        }
    }

    override fun hideMarkerInfo() {
        bsBehavior.state = BottomSheetBehavior.STATE_HIDDEN
    }

    override fun updateSeen() {
        lastShownItem?.let {
            notseenView.visibility = if (presenter.seen(it)) View.INVISIBLE else View.VISIBLE
        }
    }

    private fun initMapFragment() = findMapFragment().getMapAsync(this)

    private fun findMapFragment() = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment

    @SuppressLint("MissingPermission")
    private fun initMapControls() {
        map.uiSettings.isZoomControlsEnabled = true
        map.setMaxZoomPreference(17f)
        map.setMinZoomPreference(14f)
        checkAndRequestPermissions(REQUEST_CODE) { enableMyLocation() }
    }

    private fun initBsBehaviour() {
        bsBehavior = BottomSheetBehavior.from(bottomSheet)
        bsBehavior.setBottomSheetCallback(object : StateChangedCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    presenter.onSelectedItemRemove()
                }
            }
        })
    }

    private fun setMapListeners() {
        map.setOnMarkerClickListener(this)
        map.setOnCameraIdleListener(this)
    }

    @SuppressLint("MissingPermission")
    private fun enableMyLocation() {
        map.isMyLocationEnabled = true
        presenter.onLocationEnabled()
    }

    companion object {
        const val REQUEST_CODE = 1

        fun newInstance() = MapFragment()
    }
}