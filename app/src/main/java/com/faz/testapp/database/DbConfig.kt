package com.faz.testapp.database

object DbConfig {
    const val CACHE_EXPIRATION_MILLIS = 10 * 60 * 1000 // 10 min in milliseconds
}