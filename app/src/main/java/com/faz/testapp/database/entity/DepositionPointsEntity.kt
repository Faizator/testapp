package com.faz.testapp.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.faz.testapp.api.data.DepositionPointsDTO
import com.faz.testapp.database.DbConfig.CACHE_EXPIRATION_MILLIS
import com.faz.testapp.database.entity.DepositionPointsEntity.Companion.TABLE_NAME
import com.google.gson.Gson
import kotlin.math.roundToInt

@Entity(tableName = TABLE_NAME)
data class DepositionPointsEntity(
        @ColumnInfo(name = ID) @PrimaryKey val id: String,
        @ColumnInfo(name = DATA) val data: String,
        @ColumnInfo(name = EXPIRATION_TIME) val expirationTime: Long
) {

    companion object {
        private val gson = Gson()

        const val TABLE_NAME = "deposition_points"
        const val ID = "id"
        const val DATA = "data"
        const val EXPIRATION_TIME = "expiration_time"

        private fun roundOff(value: Double): Double {
            return (value * 100.0).roundToInt() / 100.0
        }

        fun calcId(latitude: Double, longitude: Double, radius: Long): String {
            val lat = roundOff(latitude)
            val long = roundOff(longitude)
            return "$lat:$long:$radius"
        }

        fun deserializeData(data: String): DepositionPointsDTO = gson.fromJson(data, DepositionPointsDTO::class.java)

        fun from(latitude: Double, longitude: Double, radius: Long, response: DepositionPointsDTO): DepositionPointsEntity {
            val id = calcId(latitude, longitude, radius)
            val data = gson.toJson(response)
            return DepositionPointsEntity(id, data, System.currentTimeMillis() + CACHE_EXPIRATION_MILLIS)
        }
    }
}
