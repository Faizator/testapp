package com.faz.testapp.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.faz.testapp.database.entity.SeenDepositionPointsEntity.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class SeenDepositionPointsEntity(
        @ColumnInfo(name = ID) @PrimaryKey val id: String
) {
    companion object {
        const val TABLE_NAME = "seen_deposition_points"
        const val ID = "id"

        fun calcId(latitude: Double, longitude: Double, externalId: String): String {
            return "$latitude:$longitude:$externalId"
        }

        fun create(latitude: Double, longitude: Double, externalId: String) = SeenDepositionPointsEntity(calcId(latitude, longitude, externalId))
    }
}
