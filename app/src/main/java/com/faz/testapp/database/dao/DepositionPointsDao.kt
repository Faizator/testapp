package com.faz.testapp.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.faz.testapp.database.entity.DepositionPointsEntity
import com.faz.testapp.database.entity.DepositionPointsEntity.Companion.TABLE_NAME
import com.faz.testapp.database.entity.DepositionPointsEntity.Companion.ID
import com.faz.testapp.database.entity.DepositionPointsEntity.Companion.EXPIRATION_TIME
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface DepositionPointsDao {

    @Insert(onConflict = REPLACE)
    fun insert(entity: DepositionPointsEntity): Completable

    @Query("SELECT * FROM $TABLE_NAME WHERE $ID = :id AND $EXPIRATION_TIME >= :expirationTime")
    fun getById(id: String, expirationTime: Long = System.currentTimeMillis()): Single<DepositionPointsEntity>
}