package com.faz.testapp.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.faz.testapp.api.data.DepositionPartnersDTO
import com.faz.testapp.database.DbConfig.CACHE_EXPIRATION_MILLIS
import com.faz.testapp.database.entity.DepositionPartnersEntity.Companion.TABLE_NAME
import com.google.gson.Gson

@Entity(tableName = TABLE_NAME)
data class DepositionPartnersEntity(
        @ColumnInfo(name = DATA) val data: String,
        @ColumnInfo(name = EXPIRATION_TIME) val expirationTime: Long,
        @ColumnInfo(name = ID) @PrimaryKey val id: Long = 1
) {

    companion object {
        private val gson = Gson()

        const val TABLE_NAME = "deposition_partners"
        const val ID = "id"
        const val DATA = "data"
        const val EXPIRATION_TIME = "expiration_time"

        fun deserializeData(data: String): DepositionPartnersDTO = gson.fromJson(data, DepositionPartnersDTO::class.java)

        fun from(response: DepositionPartnersDTO): DepositionPartnersEntity {
            val data = gson.toJson(response)
            return DepositionPartnersEntity(data = data, expirationTime = System.currentTimeMillis() + CACHE_EXPIRATION_MILLIS)
        }
    }
}
