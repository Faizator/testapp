package com.faz.testapp.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.Query
import com.faz.testapp.database.entity.SeenDepositionPointsEntity
import com.faz.testapp.database.entity.SeenDepositionPointsEntity.Companion.ID
import com.faz.testapp.database.entity.SeenDepositionPointsEntity.Companion.TABLE_NAME
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface SeenDepositionPointsDao {

    @Insert(onConflict = IGNORE)
    fun insert(entity: SeenDepositionPointsEntity): Completable

    @Query("SELECT * FROM $TABLE_NAME WHERE $ID = :id")
    fun getById(id: Int): Single<SeenDepositionPointsEntity>

    @Query("SELECT * FROM $TABLE_NAME")
    fun getAll(): Single<List<SeenDepositionPointsEntity>>
}