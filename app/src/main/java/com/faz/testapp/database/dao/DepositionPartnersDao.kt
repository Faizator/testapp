package com.faz.testapp.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.faz.testapp.database.entity.DepositionPartnersEntity
import com.faz.testapp.database.entity.DepositionPartnersEntity.Companion.TABLE_NAME
import com.faz.testapp.database.entity.DepositionPartnersEntity.Companion.EXPIRATION_TIME
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface DepositionPartnersDao {

    @Insert(onConflict = REPLACE)
    fun insert(entity: DepositionPartnersEntity): Completable

    @Query("SELECT * FROM $TABLE_NAME WHERE $EXPIRATION_TIME >= :expirationTime LIMIT 1")
    fun get(expirationTime: Long = System.currentTimeMillis()): Single<DepositionPartnersEntity>
}