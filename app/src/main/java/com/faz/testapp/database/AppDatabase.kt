package com.faz.testapp.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.faz.testapp.database.dao.DepositionPartnersDao
import com.faz.testapp.database.dao.DepositionPointsDao
import com.faz.testapp.database.dao.SeenDepositionPointsDao
import com.faz.testapp.database.entity.DepositionPartnersEntity
import com.faz.testapp.database.entity.DepositionPointsEntity
import com.faz.testapp.database.entity.SeenDepositionPointsEntity

@Database(entities = [
    DepositionPointsEntity::class,
    SeenDepositionPointsEntity::class,
    DepositionPartnersEntity::class
], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getDepositionPointsDao(): DepositionPointsDao

    abstract fun getSeenDepositionPointsDao(): SeenDepositionPointsDao

    abstract fun getDepositionPartnersDao(): DepositionPartnersDao
}