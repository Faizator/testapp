package com.faz.testapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.faz.testapp.R
import com.faz.testapp.data.ListItemData
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item.*

class ListAdapter(
    private val seenChecker: (ListItemData) -> Boolean,
    private val clickListener: (ListItemData, ImageView) -> Unit
) : RecyclerView.Adapter<ListAdapter.PointViewHolder>() {

    var items = listOf<ListItemData>()
        set(points) {
            field = points
            notifyDataSetChanged()
        }

    fun getItem(position: Int) = items[position]

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PointViewHolder(inflateView(parent))

    private fun inflateView(parent: ViewGroup) = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)

    override fun onBindViewHolder(viewHolder: PointViewHolder, position: Int) = viewHolder.bind(items[position])

    override fun getItemCount() = items.size

    inner class PointViewHolder internal constructor(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(item: ListItemData) {
            val partnerName = item.partner.name
            val address = item.point.fullAddress
            notseenView.visibility = if (seenChecker(item)) View.INVISIBLE else View.VISIBLE
            nameView.text = partnerName
            addressView.text = address
            Glide.with(itemView)
                    .load(item.iconUrl)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(iconView)

            itemView.setOnClickListener {
                clickListener(item, iconView)
                notseenView.visibility = View.INVISIBLE
            }
        }
    }
}