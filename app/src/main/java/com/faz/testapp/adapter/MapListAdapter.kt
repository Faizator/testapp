package com.faz.testapp.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.faz.testapp.fragment.ListFragment
import com.faz.testapp.fragment.MapFragment

class MapListAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    private val titles = listOf("Карта", "Список")

    override fun getItem(position: Int): Fragment {
        if (position == 0) {
            return MapFragment.newInstance()
        }
        return ListFragment.newInstance()
    }

    override fun getCount() = 2

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }
}