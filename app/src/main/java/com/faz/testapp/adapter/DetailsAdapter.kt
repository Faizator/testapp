package com.faz.testapp.adapter

import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.faz.testapp.R
import com.faz.testapp.data.DetailsParamData
import com.faz.testapp.data.ListItemData
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.details_item.*

class DetailsAdapter : RecyclerView.Adapter<DetailsAdapter.ParamViewHolder>() {

    private val items = mutableListOf<DetailsParamData>()

    fun getItem(position: Int) = items[position]

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ParamViewHolder(inflateView(parent))

    private fun inflateView(parent: ViewGroup) = LayoutInflater.from(parent.context).inflate(R.layout.details_item, parent, false)

    override fun onBindViewHolder(viewHolder: ParamViewHolder, position: Int) = viewHolder.bind(items[position])

    override fun getItemCount() = items.size

    fun fillWithItem(item: ListItemData) {
        val fullAddress = item.point.fullAddress
        val isMomentary = item.partner.isMomentary
        val limitations = Html.fromHtml(item.partner.limitations).toString()
        val description = Html.fromHtml(item.partner.description).toString()
        val url = item.partner.url
        items.add(DetailsParamData("Адрес", fullAddress))
        items.add(DetailsParamData("Мгновенный перевод", if (isMomentary) "Да" else "Нет"))
        items.add(DetailsParamData("Ограничения", limitations))
        items.add(DetailsParamData("Доп. информация", description))
        items.add(DetailsParamData("Сайт", url))
        notifyDataSetChanged()
    }

    inner class ParamViewHolder internal constructor(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(data: DetailsParamData) {
            paramNameView.text = data.name
            paramValueView.text = data.value
        }
    }
}