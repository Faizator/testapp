package com.faz.testapp.presenter

import android.annotation.SuppressLint
import android.location.Criteria
import android.location.Location
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.faz.gameoflife.common.kextension.addTo
import com.faz.testapp.common.ServiceProvider
import com.faz.testapp.interactor.MapListInteractor
import com.faz.testapp.view.MapView
import com.faz.testapp.data.ListItemData
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.VisibleRegion
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

@InjectViewState
class MapPresenter @Inject constructor(
    private val serviceProvider: ServiceProvider,
    private val interactor: MapListInteractor
) : MvpPresenter<MapView>() {

    private val compositeDisposable = CompositeDisposable()

    override fun onFirstViewAttach() {
        viewState.hideMarkerInfo()

        interactor.seenChange().subscribe {
            viewState.updateSeen()
        }.addTo(compositeDisposable)
    }

    fun seen(item: ListItemData) = interactor.seen(item)

    @SuppressLint("MissingPermission")
    fun onLocationEnabled() {
        val locationManager = serviceProvider.locationManager() ?: return
        val providers = locationManager.getProviders(true)
        var bestLocation: Location? = null
        for (provider in providers) {
            val location = locationManager.getLastKnownLocation(provider) ?: continue
            if (bestLocation == null || location.accuracy < bestLocation.accuracy) {
                bestLocation = location
            }
        }
        bestLocation?.let {
            viewState.moveTo(
                LatLng(
                    it.latitude,
                    it.longitude
                )
            )
        }
    }

    fun onVisibleRegionChanged(vr: VisibleRegion) {
        val top = vr.latLngBounds.northeast.latitude

        val center = Location("center")
        center.latitude = vr.latLngBounds.center.latitude
        center.longitude = vr.latLngBounds.center.longitude

        val topCenterLocation = Location("topCenter")
        topCenterLocation.latitude = top
        topCenterLocation.longitude = center.longitude

        val radius = center.distanceTo(topCenterLocation).toLong()
        print(radius)

        interactor.points(center.latitude, center.longitude, radius)
            .subscribe({
                viewState.addDepositionPoints(it)
            }, {
                print(it)
            }).addTo(compositeDisposable)
    }

    fun onItemSelect(item: ListItemData) {
        viewState.showMarkerInfo(item)
    }

    fun onSelectedItemRemove() {
        viewState.hideMarkerInfo()
    }

    fun onItemClick(item: ListItemData) {
        interactor.markAsSeen(item)
    }
}