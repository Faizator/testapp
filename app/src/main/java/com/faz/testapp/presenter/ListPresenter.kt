package com.faz.testapp.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.faz.gameoflife.common.kextension.addTo
import com.faz.testapp.interactor.MapListInteractor
import com.faz.testapp.view.ListView
import com.faz.testapp.data.ListItemData
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

@InjectViewState
class ListPresenter @Inject constructor(
    private val interactor: MapListInteractor
) : MvpPresenter<ListView>() {

    private val compositeDisposable = CompositeDisposable()

    override fun onFirstViewAttach() {
        interactor.currentPointsChange().subscribe {
            viewState.showPoints(it)
        }.addTo(compositeDisposable)

        interactor.seenChange().subscribe {
            viewState.updateSeen()
        }.addTo(compositeDisposable)
    }

    fun onItemClick(item: ListItemData) = interactor.markAsSeen(item)

    fun seen(item: ListItemData) = interactor.seen(item)

    override fun onDestroy() = compositeDisposable.dispose()
}