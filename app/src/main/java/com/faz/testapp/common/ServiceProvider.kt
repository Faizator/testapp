package com.faz.testapp.common

import android.content.Context
import android.location.LocationManager
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ServiceProvider @Inject constructor(
    private val context: Context
) {
    fun locationManager() = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager?
}