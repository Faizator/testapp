package com.faz.testapp.common.rx

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SchedulerProvider @Inject constructor() {
    fun io() = Schedulers.io()

    fun computation() = Schedulers.computation()

    fun commonSingle() = Schedulers.single()

    fun ui() = AndroidSchedulers.mainThread()
}