package com.faz.testapp.common.kextension

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment

private val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)

fun Fragment.checkAndRequestPermissions(requestCode: Int, processIfGranted: () -> Unit) =
    when {
        hasAllRequiredPermissions() -> processIfGranted()
        else -> requestPermissions(
            permissions,
            requestCode
        )
    }

fun Fragment.hasAllRequiredPermissions() =
    Build.VERSION.SDK_INT < 23 || isPermissionsGranted(requireContext(), permissions)

fun isPermissionsGranted(context: Context, permissions: Array<String>) =
    !permissions.map { ContextCompat.checkSelfPermission(context, it) }.contains(PackageManager.PERMISSION_DENIED)