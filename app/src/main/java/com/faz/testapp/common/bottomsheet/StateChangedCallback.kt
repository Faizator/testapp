package com.faz.testapp.common.bottomsheet

import android.view.View
import com.google.android.material.bottomsheet.BottomSheetBehavior

abstract class StateChangedCallback : BottomSheetBehavior.BottomSheetCallback() {
    override fun onSlide(bottomSheet: View, slideOffset: Float) {
        // ignored
    }
}