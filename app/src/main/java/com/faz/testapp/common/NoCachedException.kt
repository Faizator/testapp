package com.faz.testapp.common

import java.lang.Exception

class NoCachedException : Exception("No cached version")