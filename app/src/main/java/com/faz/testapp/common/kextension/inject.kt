package com.faz.testapp.common.kextension

import com.faz.testapp.App
import com.faz.testapp.di.component.AppComponent

val appComponent get() = App.instance.appComponent

fun <T> inject(func: AppComponent.() -> T) = lazy { appComponent.func() }