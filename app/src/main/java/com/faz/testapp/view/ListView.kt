package com.faz.testapp.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.faz.testapp.data.ListItemData

@StateStrategyType(AddToEndSingleStrategy::class)
interface ListView : MvpView {

    fun showPoints(points: List<ListItemData>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun updateSeen()
}