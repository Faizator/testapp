package com.faz.testapp.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleTagStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.faz.testapp.data.ListItemData
import com.google.android.gms.maps.model.LatLng

const val MARKER_INFO_TAG = "MARKER_INFO_TAG"

@StateStrategyType(OneExecutionStateStrategy::class)
interface MapView : MvpView {

    fun moveTo(location: LatLng)

    fun addDepositionPoints(points: List<ListItemData>)

    @StateStrategyType(AddToEndSingleTagStrategy::class, tag = MARKER_INFO_TAG)
    fun showMarkerInfo(item: ListItemData)

    @StateStrategyType(AddToEndSingleTagStrategy::class, tag = MARKER_INFO_TAG)
    fun hideMarkerInfo()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun updateSeen()
}