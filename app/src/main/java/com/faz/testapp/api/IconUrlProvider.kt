package com.faz.testapp.api

import android.content.Context
import android.util.DisplayMetrics.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class IconUrlProvider @Inject constructor(
    private val context: Context
) {
    private val resources by lazy { context.resources }

    private val densityParam by lazy {
        when (resources.displayMetrics.densityDpi) {
            DENSITY_MEDIUM -> "mdpi"
            DENSITY_HIGH -> "hdpi";
            DENSITY_XHIGH -> "xhdpi";
            DENSITY_XXHIGH -> "xxhdpi";
            else -> "hdpi"
        }
    }

    fun iconUrl(iconName: String) = "https://static.tinkoff.ru/icons/deposition-partners-v3/$densityParam/$iconName"
}