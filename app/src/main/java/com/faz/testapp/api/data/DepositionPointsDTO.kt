package com.faz.testapp.api.data

import com.google.gson.annotations.SerializedName

data class DepositionPointsDTO(
    @SerializedName("resultCode") val resultCode: String,
    @SerializedName("payload") val payload: List<DepositionPointDTO>,
    @SerializedName("trackingId") val trackingId: String
)