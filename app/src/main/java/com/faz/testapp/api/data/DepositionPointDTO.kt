package com.faz.testapp.api.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DepositionPointDTO(
    @SerializedName("externalId") val externalId: String,
    @SerializedName("partnerName") val partnerName: String,
    @SerializedName("location") val location: LocationDTO,
    @SerializedName("workHours") val workHours: String?,
    @SerializedName("addressInfo") val addressInfo: String?,
    @SerializedName("fullAddress") val fullAddress: String,
    @SerializedName("verificationInfo") val verificationInfo: String?
) : Parcelable