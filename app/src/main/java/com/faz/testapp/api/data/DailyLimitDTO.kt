package com.faz.testapp.api.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DailyLimitDTO(
    @SerializedName("currency") val currency: CurrencyDTO,
    @SerializedName("amount") val amount: Long
) : Parcelable