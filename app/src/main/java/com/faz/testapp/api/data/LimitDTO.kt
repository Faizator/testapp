package com.faz.testapp.api.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LimitDTO(
    @SerializedName("currency") val currency: CurrencyDTO,
    @SerializedName("min") val min: Long,
    @SerializedName("max") val max: Long
) : Parcelable