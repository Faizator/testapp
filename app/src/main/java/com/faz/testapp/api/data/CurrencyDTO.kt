package com.faz.testapp.api.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CurrencyDTO(
    @SerializedName("code") val code: Long,
    @SerializedName("name") val name: String,
    @SerializedName("strCode") val strCode: String
) : Parcelable