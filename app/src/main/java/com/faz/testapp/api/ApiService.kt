package com.faz.testapp.api

import com.faz.testapp.api.data.DepositionPartnersDTO
import com.faz.testapp.api.data.DepositionPointsDTO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/v1/deposition_points")
    fun depositionPoints(
        @Query("latitude") latitude: Double,
        @Query("longitude") longitude: Double,
        @Query("radius") radius: Long
    ): Single<DepositionPointsDTO>

    @GET("/v1/deposition_partners?accountType=Credit")
    fun depositionPartners(): Single<DepositionPartnersDTO>
}