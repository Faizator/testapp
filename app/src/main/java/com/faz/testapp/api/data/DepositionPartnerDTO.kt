package com.faz.testapp.api.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DepositionPartnerDTO(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("picture") val picture: String,
    @SerializedName("url") val url: String,
    @SerializedName("hasLocations") val hasLocations: Boolean,
    @SerializedName("isMomentary") val isMomentary: Boolean,
    @SerializedName("depositionDuration") val depositionDuration: String,
    @SerializedName("limitations") val limitations: String,
    @SerializedName("pointType") val pointType: String,
    @SerializedName("externalPartnerId") val externalPartnerId: String,
    @SerializedName("description") val description: String,
    @SerializedName("moneyMin") val moneyMin: Long,
    @SerializedName("moneyMax") val moneyMax: Long,
    @SerializedName("hasPreferentialDeposition") val hasPreferentialDeposition: Boolean,
    @SerializedName("limits") val limits: List<LimitDTO>,
    @SerializedName("dailyLimits") val dailyLimits: List<DailyLimitDTO>
) : Parcelable