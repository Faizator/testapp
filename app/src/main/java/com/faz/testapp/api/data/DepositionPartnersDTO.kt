package com.faz.testapp.api.data

import com.google.gson.annotations.SerializedName

data class DepositionPartnersDTO(
    @SerializedName("resultCode") val resultCode: String,
    @SerializedName("payload") val payload: List<DepositionPartnerDTO>,
    @SerializedName("trackingId") val trackingId: String
)