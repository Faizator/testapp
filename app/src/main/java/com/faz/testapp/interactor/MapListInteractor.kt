package com.faz.testapp.interactor

import com.faz.testapp.api.IconUrlProvider
import com.faz.testapp.common.rx.SchedulerProvider
import com.faz.testapp.repository.DepositionRepository
import com.faz.testapp.data.ListItemData
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MapListInteractor @Inject constructor(
    private val repository: DepositionRepository,
    private val iconUrlProvider: IconUrlProvider,
    private val schedulerProvider: SchedulerProvider
) {

    private val currentPoints = BehaviorSubject.create<List<ListItemData>>()
    private val seenUpdate = PublishSubject.create<Unit>()

    fun currentPointsChange(): Observable<List<ListItemData>> = currentPoints

    fun seenChange(): Observable<Unit> = seenUpdate

    fun points(latitude: Double, longitude: Double, radius: Long): Single<List<ListItemData>> =
        repository.points(latitude, longitude, radius)
            .observeOn(schedulerProvider.computation())
            .map { points ->
                points.map {
                    val partner = repository.partner(it)
                    ListItemData(it, partner, iconUrlProvider.iconUrl(partner.picture))
                }
            }
            .observeOn(schedulerProvider.ui())
            .doOnSuccess { currentPoints.onNext(it) }

    fun markAsSeen(item: ListItemData) {
        repository.markAsSeen(item.point)
        seenUpdate.onNext(Unit)
    }

    fun seen(item: ListItemData) = repository.seen(item.point)
}