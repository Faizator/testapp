package com.faz.testapp.di.component

import com.faz.testapp.di.module.*
import com.faz.testapp.presenter.*
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ContextModule::class,
        ApiModule::class,
        StorageModule::class
    ]
)
interface AppComponent {

    val mapPresenter: MapPresenter

    val listPresenter: ListPresenter
}