package com.faz.testapp.di.module

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.room.Room
import com.faz.testapp.database.AppDatabase
import dagger.Module
import dagger.Provides

import javax.inject.Singleton

@Module
class StorageModule {

    @Provides
    @Singleton
    fun sharedPreferences(context: Context): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    @Provides
    @Singleton
    fun appDatabase(appContext: Context) =
        Room.databaseBuilder(appContext, AppDatabase::class.java, "app_database")
            .build()

}