package com.faz.testapp.di.module

import android.content.Context
import com.faz.testapp.App
import dagger.Module
import dagger.Provides

import javax.inject.Singleton

@Module
class ContextModule {

    @Provides
    @Singleton
    fun context(): Context = App.instance.applicationContext
}