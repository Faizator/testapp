package com.faz.testapp.data

import android.os.Parcelable
import com.faz.testapp.api.data.DepositionPartnerDTO
import com.faz.testapp.api.data.DepositionPointDTO
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ListItemData(
        val point: DepositionPointDTO,
        val partner: DepositionPartnerDTO,
        val iconUrl: String
) : Parcelable