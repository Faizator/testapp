package com.faz.testapp.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DetailsParamData(
        val name: String,
        val value: String
) : Parcelable